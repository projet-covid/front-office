import { Component, OnInit, Input } from '@angular/core';
import { Mood } from '../../shared/Entity/mood';
import {MoodService} from '../../shared/Service/mood.service';
import {MatSnackBar} from '@angular/material/snack-bar';

@Component({
  selector: 'app-mood-user',
  templateUrl: './mood-user.component.html',
  styleUrls: ['./mood-user.component.scss'],
})
export class MoodUserComponent implements OnInit {
  
  @Input() lstMoods: Array<Mood>;
  @Input() currentMood: number;

  constructor(private moodService: MoodService, private _snackBar: MatSnackBar) { }

  ngOnInit() {
  }

  onSubmit(){
    var newMood = this.currentMood;

    this.moodService.AddMoodChange(this.currentMood).then((data)=>{
          if(data['id']!=null){
            this._snackBar.open('L\'humeur a été modifiée', 'Ok', {duration: 2000,});
          }
          else{
            this._snackBar.open('Erreur dans la mise à jour de l\'humeur', 'Ok', {duration: 2000});
          }
      });
  }
}
