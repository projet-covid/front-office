import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { MoodUserComponent } from './mood-user.component';

describe('MoodUserComponent', () => {
  let component: MoodUserComponent;
  let fixture: ComponentFixture<MoodUserComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MoodUserComponent ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(MoodUserComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
