import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ReactiveFormsModule} from '@angular/forms';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatInputModule} from '@angular/material/input';
import {MatCardModule} from '@angular/material/card';
import {MatCheckboxModule} from '@angular/material/checkbox';
import {MatIconModule} from '@angular/material/icon';
import {MatButtonModule} from '@angular/material/button';
import {FormsModule} from '@angular/forms';
import {MatTabsModule} from '@angular/material/tabs';
import {MatSelectModule} from '@angular/material/select';
import {MatRadioModule} from '@angular/material/radio';
import {MatListModule} from '@angular/material/list';
import {MatGridListModule} from '@angular/material/grid-list';
import {IonicModule} from '@ionic/angular';
import {RouterModule, Router} from '@angular/router';
import {MatDialogModule, MatDialog} from "@angular/material";
import { HttpClientModule } from '@angular/common/http';
import { GifComponent } from '../wall/new-publication/gif/gif.component';
import {MatSnackBarModule, MatSnackBar} from '@angular/material/snack-bar';
import {MatStepperModule} from '@angular/material/stepper';
import {MoodUserComponent} from './mood-user/mood-user.component';
import { PasswordUserComponent } from './password-user/password-user.component';
import { PersonService } from './Service/person.service';
import { FacilityService } from './Service/facility.service';
import { LoginService } from './Service/login.service';
import { MoodService } from './Service/mood.service';
import { PostService } from './Service/post.service';
import { Camera } from '@ionic-native/camera/ngx';
import { PhotoLibrary } from '@ionic-native/photo-library/ngx';

@NgModule({
  declarations: [GifComponent, MoodUserComponent, PasswordUserComponent],
  imports: [
    CommonModule,
    MatFormFieldModule,
    MatInputModule,
    MatCheckboxModule,
    ReactiveFormsModule,
    MatIconModule,
    MatButtonModule,
    MatTabsModule,
    MatSelectModule,
    MatRadioModule,
    MatListModule,
    MatGridListModule,
    MatCardModule,
    MatDialogModule,
    MatSnackBarModule,
    IonicModule,
    RouterModule,
    HttpClientModule,
    FormsModule,
    MatStepperModule,
  ],
  exports: [
    MatFormFieldModule,
    MatInputModule,
    MatCheckboxModule,
    ReactiveFormsModule,
    MatIconModule,
    MatCardModule,
    MatButtonModule,
    FormsModule,
    MatDialogModule,
    MatTabsModule,
    MatSelectModule,
    MatRadioModule,
    MatListModule,
    MatGridListModule,
    MatSnackBarModule,
    IonicModule,
    RouterModule,
    HttpClientModule,
    MatStepperModule,
    MoodUserComponent,
    PasswordUserComponent
  ],
  providers: [
    PersonService,
    FacilityService,
    LoginService,
    MoodService,
    PostService,
    MatSnackBar,
    Camera,
    PhotoLibrary,
    MatDialog
  ]
})
export class SharedModule { }
