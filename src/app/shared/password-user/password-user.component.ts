import { Component, OnInit } from '@angular/core';
import { PersonService } from '../../shared/Service/person.service';
import { FormGroup, FormControl, Validators} from '@angular/forms';
import {MatSnackBar} from '@angular/material/snack-bar';

@Component({
  selector: 'app-password-user',
  templateUrl: './password-user.component.html',
  styleUrls: ['./password-user.component.scss'],
})
export class PasswordUserComponent implements OnInit {
  passwordData = new FormGroup({
    currentPassword: new FormControl('',[Validators.required]),
    newPassword: new FormControl('',[Validators.required]),
    newPasswordConfirm: new FormControl('',[Validators.required]),
  });

public hideN: boolean = false;
public hideA: boolean = false;
public hide: boolean = false;

constructor(private personService : PersonService, private _snackBar: MatSnackBar ) { }

ngOnInit() {}

onSubmit(passwordData) {
  if(this.passwordData.get('newPassword').value === this.passwordData.get('newPasswordConfirm').value){
    this.personService.changePassword(passwordData.currentPassword,passwordData.newPassword,passwordData.newPasswordConfirm).then((data)=>{
        if(data['success']){
          this._snackBar.open('Le mot de passe a été modifié', 'Ok', {duration: 2000,});
          this.passwordData = new FormGroup({
            currentPassword: new FormControl('',[Validators.required]),
            newPassword: new FormControl('',[Validators.required]),
            newPasswordConfirm: new FormControl('',[Validators.required]),
        });

        }
        else{
          //TODO let erreur = data.json();
          this._snackBar.open("Erreur dans la mise à jour du mot de passe", 'Ok', {duration: 2000});
        }
    });
  }
  else{
    this._snackBar.open('Les mots de passe doivent correspondre', 'Ok', {duration: 2000});
  }
}

}
