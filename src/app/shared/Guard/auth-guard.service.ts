import { Injectable } from '@angular/core';
import { CanActivate, Router, ActivatedRoute, RouterStateSnapshot, ActivatedRouteSnapshot } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthGuardService implements CanActivate {

  private token : string = localStorage.getItem("token");
  constructor(private router : Router) {}
  
  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot)
  {
    if(this.token != null)
    {
      return true;
    }
    else
    {
      this.router.navigate(['/connexion']);
      return false;   
    }    
  }
}
