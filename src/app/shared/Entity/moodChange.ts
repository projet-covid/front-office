import { BaseEntity } from './baseEntity';
import { IonDatetime } from '@ionic/angular';

/**
 * Class moodChange
 */
export class MoodChange extends BaseEntity {
    public createdAt?: IonDatetime;
    public mood_id?: number;
    public person_id? : number; 
}