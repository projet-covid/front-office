import { Person } from './person';
import { Post } from './post';

export class Report {
    public person?: Person;
    public post?: Post;
}
