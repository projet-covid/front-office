import { Facility } from './facility';
import { Post } from './post';

export class PostFacility {
    public facility?: Facility;
    public post?: Post;
}
