import { BaseEntity } from './baseEntity';
import { Group } from './group';

/**
 * Class facility
 */
export class Facility extends BaseEntity {
    public label?: string;
    public group?: Group;
}
