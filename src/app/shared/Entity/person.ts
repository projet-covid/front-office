import { BaseEntity } from './baseEntity';
import { IonDatetime } from '@ionic/angular';
import { Role } from './role';
import { Group } from './group';
import { Mood } from './mood';
import { Facility } from './facility';

/**
 * Class person
 */
export class Person extends BaseEntity {
    public lastName?: string;
    public firstName?: string;
    public username?: string;
    public password?: string;
    public lastLogged?: IonDatetime;
    public archived?: boolean;
    public role?: Role;
    public group?: Group;
    public mood?: Mood;
    public facilities?: Facility[];
}
