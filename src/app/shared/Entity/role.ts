import { BaseEntity } from './baseEntity';

/**
 * Class person
 */
export class Role extends BaseEntity {
    public label?: string;
}