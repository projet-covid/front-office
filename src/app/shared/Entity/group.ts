import { BaseEntity } from './baseEntity';

/**
 * Class group
 */
export class Group extends BaseEntity {
    public label: string;    
}