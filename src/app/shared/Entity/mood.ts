import { BaseEntity } from './baseEntity';

/**
 * Class mood
 */
export class Mood extends BaseEntity {
    public label?: string;
    public icon?: string;   
}