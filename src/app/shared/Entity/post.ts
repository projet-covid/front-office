import { BaseEntity } from './baseEntity';
import { Person } from './person';
import { Mood } from './mood';
import { Like } from './like';
import { Report } from './report';
import { Facility } from './facility';

/**
 * Class post
 */
export class Post extends BaseEntity {
    public content?: string;
    public archived?: boolean;
    public mood?: Mood;
    public person?: Person;
    public likes?:Like[];
    public reports?:Report[];
    public liked?:boolean;
    public reported?:boolean;
    public mediaUrl?: string;
    public facilities?: Array<Facility>;
    public publishedAt?: Date;
}
