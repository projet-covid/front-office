import { Facility } from './facility';

export class PersonFacility {
    public facility?: Facility;
    public personId?: number;
}
