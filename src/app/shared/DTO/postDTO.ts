import { Facility } from '../Entity/facility';

/**
 * Class person
 */
export class PostDTO {
    public content?: string;
    public mood?: string;
    public mediaUrl?: string;
    public facilities?: Array<Facility>;
}
