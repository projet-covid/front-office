import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { Http, RequestOptions,Headers } from '@angular/http';


@Injectable({
  providedIn: 'root'
})
export class MoodService {

  private url = environment.API_ENDPOINT_URL + 'moods';
  private urlMoodChanges = environment.API_ENDPOINT_URL + 'mood_changes';
  private token : string = localStorage.getItem("token");

  constructor(private http: Http) { }

  public getAll(){
    let headers = new Headers({
      'Access-Control-Allow-Origin': '*',
      'Authorization': 'bearer ' + this.token
    });
    let options = new RequestOptions({headers:headers});

    return new Promise((resolve, reject) => {
      this.http.get(this.url,options).toPromise()
        .then((response) => {         
          resolve(response.json());
        })
        .catch((error) => {
          resolve(null);
        });
    });
  }

  public getById(id: number)
  {
    let headers = new Headers({
      'Access-Control-Allow-Origin': '*',
      'Authorization': 'bearer ' + this.token
    });
    let options = new RequestOptions({headers:headers});

    return new Promise((resolve, reject) => {
      this.http.get(this.url+"?id="+id,options).toPromise()
        .then((response) => {
          resolve(response.json());
        })
        .catch((error) => {
          resolve(null);
        });
    });
  }

  public getCurrentMood( id : number){
    let headers = new Headers({
      'Access-Control-Allow-Origin': '*',
      'Authorization': 'bearer ' + this.token
    });
    let options = new RequestOptions({headers:headers});

    return new Promise((resolve, reject) => {
      this.http.get(this.urlMoodChanges,options).toPromise()
        .then((response) => {
          resolve(response.json()['hydra:member'][0].mood);
        })
        .catch((error) => {
          resolve(null);
        });
    });
  }

  public AddMoodChange(idMood : number){
    let headers = new Headers({
      'Access-Control-Allow-Origin': '*',
      'Authorization': 'bearer ' + this.token
    });
    let options = new RequestOptions({headers:headers});

    let data = {
      "mood": "/moods/"+idMood
    }

    return new Promise((resolve, reject) => {
      this.http.post(this.urlMoodChanges,data,options).toPromise()
        .then((response) => {
          resolve(response.json());
        })
        .catch((error) => {
          resolve(null);
        });
    });
  }
}
