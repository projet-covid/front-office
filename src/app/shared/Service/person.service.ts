import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { Http, RequestOptions,Headers } from '@angular/http';
import { Person } from '../Entity/person';

@Injectable({
  providedIn: 'root'
})
export class PersonService {

  private url = environment.API_ENDPOINT_URL + 'people';
  private urlPassword = environment.API_ENDPOINT_URL + 'change_password';
  private token : string = localStorage.getItem("token");

  constructor(private http: Http) { }

  public getById(id: number)
  {
    let headers = new Headers({
      'Access-Control-Allow-Origin': '*',
      'Authorization': 'bearer ' + this.token
    });
    let options = new RequestOptions({headers:headers});

    return new Promise((resolve, reject) => {
      this.http.get(this.url+"/"+id,options).toPromise()
        .then((response) => {
          resolve(response.json());
        })
        .catch((error) => {
          resolve(new Person());
        });
    });
  }

  public changePassword(currentPassword : string, newPassword: string, newPasswordConfirm: string)
  {
    let headers = new Headers({
      'Access-Control-Allow-Origin': '*',
      'Authorization': 'bearer ' + this.token
    });
    let options = new RequestOptions({headers:headers});

    let data = {
      "currentPassword": currentPassword,
      "newPassword": newPassword,
      "newPasswordConfirm": newPassword
    }

    return new Promise((resolve, reject) => {
      this.http.post(this.urlPassword,data ,options).toPromise()
        .then((response) => {
          resolve(response.json());
        })
        .catch((error) => {
          resolve(error);
        });
    });
  }

}
