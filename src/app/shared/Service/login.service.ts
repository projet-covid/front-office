import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http';
import { Person } from '../Entity/person';
import { environment } from 'src/environments/environment';


@Injectable({
  providedIn: 'root'
})
export class LoginService {

  private url: string = environment.API_ENDPOINT_URL + "authentication_token";

  constructor(private http: Http) { }

  public login(Person: Person)
  {
    var authent = {"username" : Person.username, "password" : Person.password};
    let headers = new Headers({ 
      'Access-Control-Allow-Origin':'*',
      'Content-Type':'application/json'
    });
    let options = new RequestOptions({headers:headers});
    
    return new Promise((resolve, reject) => {
      this.http.post(this.url, authent, options).toPromise()
        .then((response) => {
          resolve(response.json().token);
        })
        .catch((error) => {
          resolve(null);
        });
    });
  }
}
