import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { Http, Headers, RequestOptions } from '@angular/http';
import { PostDTO } from '../DTO/postDTO';



@Injectable({
  providedIn: 'root'
})
export class PostService {
  private urlFacilty = environment.API_ENDPOINT_URL +'facilities';
  private url = environment.API_ENDPOINT_URL +'posts';

  private token : string = localStorage.getItem("token");

  constructor(private http: Http) { }
  public getAllByFacility(id : number)
  {
    let headers = new Headers({ 
      'Access-Control-Allow-Origin':'*',
      'Authorization': 'bearer ' + this.token
    });
    let options = new RequestOptions({headers:headers});

    return new Promise((resolve, reject) => {
      this.http.get(this.urlFacilty+"/"+id+"/posts?page=1", options).toPromise()
        .then((response) => {
          resolve(response.json());
        })
        .catch((error) => {
          resolve(null);
        });
    });
  }

  public getAllByFacilityUrl(url : string)
  {
    let headers = new Headers({ 
      'Access-Control-Allow-Origin':'*',
      'Authorization': 'bearer ' + this.token
    });
    let options = new RequestOptions({headers:headers});

    return new Promise((resolve, reject) => {
      this.http.get(environment.API_ENDPOINT_URL + url.substring(1) , options).toPromise()
        .then((response) => {
          resolve(response.json());
        })
        .catch((error) => {
          resolve(null);
        });
    });
  }

  public AddPost(post : PostDTO)
  {
    
    let headers = new Headers({ 
      'Access-Control-Allow-Origin':'*',
      'Authorization': 'bearer ' + this.token
    });
    let options = new RequestOptions({headers:headers});

    return new Promise((resolve, reject) => {
      this.http.post(this.url,post, options).toPromise()
        .then((response) => {
          resolve(response.json());
        })
        .catch((error) => {
          resolve(null);
        });
    });
  }

  public likeOrdisLike(idPost:number, likeOrDislike:boolean)
  {
    let headers = new Headers({ 
      'Access-Control-Allow-Origin':'*',
      'Authorization': 'bearer ' + this.token,
      'Content-Type' : 'application/json'
    });
    let options = new RequestOptions({headers:headers});

    return new Promise((resolve, reject) => {
      this.http.post(this.url+"/"+idPost+"/like",{"like": likeOrDislike }, options).toPromise()
        .then((response) => {
          resolve(response.json());
        })
        .catch((error) => {
          resolve(null);
        });
    });
  }

  public reportPost(idPost:number, isReport:boolean)
  {
    let headers = new Headers({ 
      'Access-Control-Allow-Origin':'*',
      'Authorization': 'bearer ' + this.token,
      'Content-Type' : 'application/json'
    });
    let options = new RequestOptions({headers:headers});

    return new Promise((resolve, reject) => {
      this.http.post(this.url+"/"+idPost+"/report",{"report": isReport }, options).toPromise()
        .then((response) => {
          resolve(response.json());
        })
        .catch((error) => {
          resolve(null);
        });
    });
  }

}
