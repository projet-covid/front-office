import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AuthRoutingModule } from './auth-routing.module';
import { IonicModule } from '@ionic/angular';
import { SharedModule } from '../shared/shared.module';
import { LoginComponent } from './login/login.component';
import { FirstLoginComponent } from './first-login/first-login.component';






@NgModule({
  declarations: [LoginComponent, FirstLoginComponent],
  imports: [
    CommonModule, 
    IonicModule, 
    AuthRoutingModule,
    SharedModule,

  ],
})
export class AuthModule { }




