import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators , FormControl} from '@angular/forms';
import {MoodService} from '../../shared/Service/mood.service';
import { Router } from '@angular/router';
import { Mood } from '../../shared/Entity/mood';


@Component({
  selector: 'app-first-login',
  templateUrl: './first-login.component.html',
  styleUrls: ['./first-login.component.scss'],
})
export class FirstLoginComponent implements OnInit {
  isLinear = false;
  firstFormGroup: FormGroup;
  secondFormGroup: FormGroup;
  thirdFormGroup: FormGroup;
  isNotValid : boolean = false;
  lstMoods :Array<Mood>; 
  currentMood: number;

  
  constructor(private _formBuilder: FormBuilder, private router : Router, private moodService : MoodService ) { }

  ngOnInit() {
    this.getMoodsList();
    this.firstFormGroup = this._formBuilder.group({
      firstCtrl: ['', Validators.required]
    });
    this.secondFormGroup = this._formBuilder.group({
      secondCtrl: ['', Validators.required]
    });
    this.thirdFormGroup = this._formBuilder.group({
      thirdCtrl: ['', Validators.required],
      checkboxConditions : new FormControl(false)
    });
  }
  onSubmit(){
    var acceptCondition = document.getElementById("acceptCondition").getAttribute("aria-checked");
    if(acceptCondition == "true"){
      this.router.navigateByUrl('/mur'),{queryParams: {reload: 'true'}};
    }else{
      this.isNotValid = true;
    }
  }

  getMoodsList(){
    this.moodService.getAll().then((data) => {
      this.lstMoods = data['hydra:member'] as Array<Mood>;
    });
  }

}
