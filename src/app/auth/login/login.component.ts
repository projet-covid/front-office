import { Component, OnInit } from '@angular/core';
import { Person } from '../../shared/Entity/person';
import { LoginService } from '../../shared/Service/login.service';
import { Router } from '@angular/router';
import * as jwt_decode from 'jwt-decode';



@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit {
  PersonConnexion : Person;
  constructor(private loginService : LoginService, private router : Router)
  { 
    this.PersonConnexion = new Person();
  }

  ngOnInit() {}
  
  isNotValid : boolean = false;
  public login()
  {
    // Verification User existe et bon mot de passe & redirection vers la bonne page si succès ou non.
    this.loginService.login(this.PersonConnexion).then((token) => {
      const ident = token as string;
      if(ident != null)
      {
        // Enregistrer l'utilisateur en globale : localStorage ? Classe static ?
        localStorage.setItem("token",ident);     
        var lastLog = jwt_decode(ident).lastLogged; 
        if(lastLog){
          this.router.navigate(['/mur'], {queryParams: {reload: 'true'}});
        }else{
          this.router.navigate(['/connexion/premiere']);
        }
      }
      else
      {
        this.isNotValid = true;
        setTimeout(()=>{
          this.isNotValid = false;
        },10000)
      }      
    });
  }
}
