import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Mood } from '../../shared/Entity/mood';
import { Person } from '../../shared/Entity/person';
import {MoodService} from '../../shared/Service/mood.service';
import {PersonService} from '../../shared/Service/person.service';
import * as jwt_decode from 'jwt-decode';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss'],
})
export class ProfileComponent implements OnInit {
  private token : string = localStorage.getItem("token");
  person : Person;
  lstMoods :Array<Mood>; 
  currentMood: number;

  constructor(private route : ActivatedRoute, private personService: PersonService, private moodService: MoodService) { }

  ngOnInit() {    
    this.getPersonConnected();
    this.getMoodsList();

    this.route.queryParamMap.subscribe(params => {
      if(params.get('reload')){
        this.getPersonConnected();      
      }
    });
  }

  getPersonConnected(){
    var userConnectId = jwt_decode(this.token).user_id; 
    this.personService.getById(userConnectId).then((data)=>{
      this.person = data as Person;
      this.currentMood = this.person.mood.id;
    });
  }

  getMoodsList(){
    this.moodService.getAll().then((data) => {
      this.lstMoods = data['hydra:member'] as Array<Mood>;
    });
  }
 
}
