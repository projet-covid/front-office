import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../shared/shared.module';

//ROUTER\\
import { SettingsRoutingModule } from './settings-routing.module';

//COMPONENTS\\
import {IonicModule} from '@ionic/angular';
import {ProfileComponent} from './profile/profile.component';
import {InformationComponent} from './information/information.component';

@NgModule({
  declarations: [
    ProfileComponent,
    InformationComponent,
  ],
  imports: [CommonModule, IonicModule, SettingsRoutingModule, SharedModule],
})
export class SettingsModule { }
