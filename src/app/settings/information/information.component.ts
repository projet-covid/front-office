import { Person } from '../../shared/Entity/person';
import { Component, OnInit,Input } from '@angular/core';

@Component({
  selector: 'app-information',
  templateUrl: './information.component.html',
  styleUrls: ['./information.component.scss'],
})
export class InformationComponent implements OnInit {
  @Input() person : Person;

  constructor() { }

  ngOnInit() {  }

}
