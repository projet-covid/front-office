import { Component } from '@angular/core';

import { Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { Router } from '@angular/router';
import * as jwt_decode from 'jwt-decode';
import { ConfirmModalComponent } from './wall/confirm-modal/confirm-modal.component';
import { MatDialog, MatDialogConfig } from '@angular/material';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent {
  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private router : Router,
    private dialog: MatDialog
  ) {
    this.initializeApp();    
    if(localStorage.getItem("token") != null)
    {
      var decoded = jwt_decode(localStorage.getItem("token"));    
      var dateNow  = Number.parseFloat((new Date().getTime() / 1000).toString());
      if(decoded.exp < dateNow)
      {      
        localStorage.removeItem("token");
      }
    }
    
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }

  public isConnexionRoute()
  {
    if(this.router.url === '/connexion' || this.router.url === '/deconnexion' || this.router.url === '/connexion/premiere')
    {
      return true;
    }
    else
    {
      return false;
    }
    
  }

  public isProfil()
  {
    return this.router.url === '/monCompte/profil';
  }

  public isWall()
  {
    return this.router.url === '/mur';
  }

  public isPublication()
  {
    return this.router.url === '/mur/publier';
  }

  public deconnexion()
  {
      this.dialog.open(ConfirmModalComponent);
  }
  
}
