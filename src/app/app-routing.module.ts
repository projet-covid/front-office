import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { AuthGuardService } from './shared/Guard/auth-guard.service';

const routes: Routes = [
  { path: 'mur', loadChildren: () => import('./wall/wall.module').then( m => m.WallModule ), canActivate: [AuthGuardService] },
  { path: 'monCompte', loadChildren: () => import('./settings/settings.module').then( m => m.SettingsModule ), canActivate: [AuthGuardService] },
  { path: 'connexion', loadChildren: () => import('./auth/auth.module').then( m => m.AuthModule ) },
  { path: 'deconnexion', loadChildren: () => import('./auth/auth.module').then( m => m.AuthModule ), canActivate: [AuthGuardService] },
  { path: '', redirectTo: 'connexion', pathMatch: 'full', canActivate: [AuthGuardService] },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
