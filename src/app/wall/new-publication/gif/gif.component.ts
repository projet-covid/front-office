import { Component, OnInit, Output, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { EventEmitter } from 'events';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'app-gif',
  templateUrl: './gif.component.html',
  styleUrls: ['./gif.component.scss'],
})
export class GifComponent implements OnInit {
  // Define the `phonecatApp` module

  @Output() close = new EventEmitter();

  constructor(private http: HttpClient, private dialogRef: MatDialogRef<GifComponent>,
    @Inject(MAT_DIALOG_DATA) data) {}

  ngOnInit() {}

  Name : string;
  gifs : any;
  load : boolean = false;
  changeGif()
  {
    var url = 'https://api.giphy.com/v1/gifs/search?q='+ this.Name +'&api_key=I4Lk2g7oevW82PKru6QRUX13tNXtSEOW';   
    this.load = true;
    this.http.get(url).subscribe((response)=>{
    this.gifs = response['data']; 
    this.load = false;
      
    });
  }

  selectedGif(gif : any)
  {
    localStorage.setItem("selectedGif", gif.images.downsized_large.url);
    this.dialogRef.close(true);
  }

  closeModal()
  {
    this.dialogRef.close(true);
    localStorage.removeItem("selectedGif");    
  }


}