import { Component, OnInit } from '@angular/core';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';
import { PhotoLibrary } from '@ionic-native/photo-library/ngx';
import { FormGroup, FormControl, Validators} from '@angular/forms';
import { MatDialog } from "@angular/material";
import { GifComponent } from './gif/gif.component';
import * as jwt_decode from 'jwt-decode';
import { PersonService } from '../../shared/Service/person.service';
import { Person } from '../../shared/Entity/person';
import { PostDTO } from '../../shared/DTO/postDTO';
import { Facility } from '../../shared/Entity/facility';
import { PostService } from '../../shared/Service/post.service';
import { Router, ActivatedRoute } from '@angular/router';


@Component({
  selector: 'app-new-publication',
  templateUrl: './new-publication.component.html',
  styleUrls: ['./new-publication.component.scss'],
})
export class NewPublicationComponent implements OnInit {

  person: Person;
  postForm = new FormGroup({
    message: new FormControl('',[
      Validators.required,
      Validators.maxLength(280)
    ]),
    withMood: new FormControl(false),
    etablissements: new FormControl('',[
      Validators.required
    ])
  });  
  private token : string = localStorage.getItem("token");
  lstEtablissement : Array<Facility> = new Array<Facility>();
    
  constructor( private route: ActivatedRoute, private camera: Camera, private photoLibrary: PhotoLibrary, private dialog: MatDialog, private personService : PersonService, private postService : PostService, private router : Router) {
  }

  ngOnInit() {
    var decoded = jwt_decode(this.token); 
    var idUtilisateur = decoded.user_id;
    this.personService.getById(idUtilisateur).then((data) => {
      this.person = data as Person;
      this.lstEtablissement = this.person.facilities;
    });

    this.route.queryParamMap.subscribe(params => {
      if(params.get('reload')){
        decoded = jwt_decode(this.token); 
        idUtilisateur = decoded.user_id;
        this.personService.getById(idUtilisateur).then((data) => {
          this.person = data as Person;
          this.lstEtablissement = this.person.facilities;
        });
      }else{
      }
  });
    
  }

  gif : any;
  selectedGif : boolean = false;
  openDialog() {
    const dialogRef = this.dialog.open(GifComponent, {
      width: '100%',
      maxWidth: '100%',
      height:'100%',
      maxHeight:'100%', 
    });
    dialogRef.afterClosed().subscribe(() => {
      this.gif = localStorage.getItem("selectedGif");
    });
  }

  takePicture() {
    const options: CameraOptions = {
      quality: 100,
      destinationType: this.camera.DestinationType.FILE_URI,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE
    }
    
    this.camera.getPicture(options).then((imageData) => {
     // imageData is either a base64 encoded string or a file URI
     // If it's base64 (DATA_URL):
     let base64Image = 'data:image/jpeg;base64,' + imageData;
     console.info('photo prise')
    }, (err) => {
      console.error('erreur photo :' + err)
    });
  }

  

  findPicture() {
    this.photoLibrary.requestAuthorization().then(() => {
      this.photoLibrary.getLibrary().subscribe({
        next: library => {
          library.forEach(function(libraryItem) {
            console.log(libraryItem.id);          // ID of the photo
            console.log(libraryItem.photoURL);    // Cross-platform access to photo
            console.log(libraryItem.thumbnailURL);// Cross-platform access to thumbnail
            console.log(libraryItem.fileName);
            console.log(libraryItem.width);
            console.log(libraryItem.height);
            console.log(libraryItem.creationDate);
            console.log(libraryItem.latitude);
            console.log(libraryItem.longitude);
            console.log(libraryItem.albumIds);    // array of ids of appropriate AlbumItem, only of includeAlbumsData was used
          });
        },
        error: err => { console.log('could not get photos'); },
        complete: () => { console.log('done getting photos'); }
      });
    })
    .catch(err => console.log('permissions weren\'t granted'));
  }

  onSubmit(customerData) {
    if (this.postForm.valid) 
    {    
      var post = new PostDTO();

      post.content = customerData.message;

      if(!customerData.withMood)
      {
        post.mood = this.person.mood["@id"];
      }
      
      post.facilities = new Array<Facility>();
      customerData.etablissements.forEach(etablissement => {        
        post.facilities.push(etablissement);
      });

      post.mediaUrl = this.gif;
      
      this.postService.AddPost(post).then((response)=>{
        this.postForm = new FormGroup({
          message: new FormControl('',[
            Validators.required,
            Validators.maxLength(280)
          ]),
          withMood: new FormControl(false),
          etablissements: new FormControl('',[
            Validators.required
          ])
        });
        this.gif = null;
      });
      this.router.navigate(['/mur'], { queryParams: { reload: 'true' } });
    }
  }

  public deleteGif()
  {
    this.gif = null;
    localStorage.removeItem("selectedGif");
  }

  get message() { return this.postForm.get('message'); }
  get withMood() { return this.postForm.get('withMood'); }
  get etablissement() {return this.postForm.get('etablissements')}

}
