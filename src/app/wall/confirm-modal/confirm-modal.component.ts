import { Component, OnInit, Inject } from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material";
import { Router } from '@angular/router';

@Component({
  selector: 'app-confirm-modal',
  templateUrl: './confirm-modal.component.html',
  styleUrls: ['./confirm-modal.component.scss'],
})
export class ConfirmModalComponent implements OnInit {

  constructor(private dialogRef: MatDialogRef<ConfirmModalComponent>,
    @Inject(MAT_DIALOG_DATA) data, private router : Router) { }

  ngOnInit() {}
  
  save() {
    localStorage.removeItem("token");
    this.router.navigate(["connexion"]);
    this.dialogRef.close(true);   
}

close() {
    this.dialogRef.close(false);
}
}
