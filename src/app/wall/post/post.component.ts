import { Component, OnInit, Input } from '@angular/core';
import { Post } from '../../shared/Entity/post';
import { PostService } from '../../shared/Service/post.service';
import { Like } from '../../shared/Entity/like';
import { Facility } from '../../shared/Entity/facility';

@Component({
  selector: 'app-post',
  templateUrl: './post.component.html',
  styleUrls: ['./post.component.scss'],
})
export class PostComponent implements OnInit {
  @Input() post:Post;
  likeCheckbox=false;
  flagmanCheckbox = false;
  public lstEtablissementUser : Array<Facility>;
  constructor( private postService : PostService) {

   }

  ngOnInit() {

    this.likeCheckbox= this.post.liked;
    
    this.flagmanCheckbox = this.post.reported;
  }

publicationSignaler(checkbox){
      //si false on selectionne le bouton like
    let idPost = checkbox.post.id;
    let signalerDis = true;
    if(checkbox.flagmanCheckbox){
      signalerDis = false;     
    } 
  this.postService.reportPost(idPost,signalerDis);

  }


  publicationLike(checkbox){
    //si false on selectionne le bouton like
  let idPost = checkbox.post.id;
  let likeOrDislike = true;

  if(checkbox.likeCheckbox){
    likeOrDislike = false;
    // -1
    if(this.post.likes.length > 0){
        this.post.likes.pop();
    }
  }
  else {
      this.post.likes.push(new Like());
  }

  this.postService.likeOrdisLike(idPost,likeOrDislike);

}

}
