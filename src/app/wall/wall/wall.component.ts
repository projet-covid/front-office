import { Component, OnInit } from '@angular/core';
import { Post } from '../../shared/Entity/post';
import { PostService } from '../../shared/Service/post.service';
import { PersonService } from '../../shared/Service/person.service';
import { Facility } from '../../shared/Entity/facility';
import * as jwt_decode from 'jwt-decode';
import { Person } from '../../shared/Entity/person';
import { ActivatedRoute } from '@angular/router';


@Component({
  selector: 'app-wall',
  templateUrl: './wall.component.html',
  styleUrls: ['./wall.component.scss'],
})
export class WallComponent implements OnInit {

  lstPost: Array<Post>;
  lstEtablissement:Array<Facility>;
  idUtilisateur :number;
  person :Person;
  idEtablissement : number;
  nextUrlPost : string;
  lastUrlPost : string;
  idEtablissementString :string;
  lastIdPostDisplay :number;
  lstPostCheckNew:Array<Post>;
  nombrePost : number;
  reload: boolean;
  private token : string = localStorage.getItem("token");

  constructor(private postService: PostService, private personService :PersonService, private route: ActivatedRoute) { 
    this.lstEtablissement = [];
    this.lstPostCheckNew = [];
    this.getNewPost = this.getNewPost.bind(this);
  }

  ngOnInit() {
    
    var decoded = jwt_decode(this.token); 
    this.idUtilisateur = decoded.user_id;    

    this.personService.getById(this.idUtilisateur).then((data) => {
      this.person = data as Person;
      
      this.lstEtablissement = this.person.facilities;
      this.idEtablissement = this.lstEtablissement[0].id;
      this.idEtablissementString =  this.lstEtablissement[0].id.toString();
      if(this.lstEtablissement.length > 0){
      }
     this.loadPostInWall(this.idEtablissement);
    });

    setInterval(this.getNewPost,1200000);    


    this.route.queryParamMap.subscribe(params => {
      if(params.get('reload')){
        this.lstPost = [];
        decoded = jwt_decode(this.token); 
        this.idUtilisateur = decoded.user_id;  
        this.personService.getById(this.idUtilisateur).then((data) => {
          this.person = data as Person;
          this.lstEtablissement = this.person.facilities;
          this.idEtablissement = this.lstEtablissement[0].id;
          this.idEtablissementString =  this.lstEtablissement[0].id.toString();
          if(this.lstEtablissement.length > 0){
          }
          this.loadPostInWall(this.idEtablissement);
        });

      }
    });

  }
  refreshPost(){
    this.postService.getAllByFacility(this.idEtablissement).then((data) => {     
      this.lstPost = []; 
      this.lstPost = data['hydra:member'] as Array<Post>;
      this.lastIdPostDisplay = this.lstPost[0].id;
      window.scrollTo(0,0);
      this.nombrePost = 0;
      document.getElementById("butonNewPost").style.display = "none";

    });
    
  }
  
  getNewPost(){
    var isNewPost = false;

    this.postService.getAllByFacility(this.idEtablissement).then((data) => {      
      this.lstPostCheckNew = data['hydra:member'] as Array<Post>;
     
    });
    var nombreNewPost = 0;
    this.lstPostCheckNew.forEach(element => {
      if(element.id > this.lastIdPostDisplay){
        nombreNewPost +=1;
        isNewPost = true;         
        
      }
    });
    
    if(isNewPost){
      document.getElementById("butonNewPost").style.display = "block";
      this.nombrePost = nombreNewPost ;
    }else{
      document.getElementById("butonNewPost").style.display = "none";
    }
  }



  loadPostInWall(pIdEtablissement){
    this.postService.getAllByFacility(pIdEtablissement).then((data) => {
      if(data['hydra:member']){
        this.lstPost = data['hydra:member'] as Array<Post>;
        if(this.lstPost.length >0){
          this.lastIdPostDisplay = this.lstPost[0].id;
        }
        if(data['hydra:view'] &&  data['hydra:view']['hydra:next']){
          this.nextUrlPost = data['hydra:view']['hydra:next'];

        }else{
          this.nextUrlPost = "";
        }
      }
     
    });
    return this.lstPost;

  }

  changeEtablissement(idEtablissement){
    this.nextUrlPost = "";
    this.lastUrlPost = "";
    this.idEtablissement = idEtablissement;
    this.lstPost = [];
    this.lstPost = this.loadPostInWall(idEtablissement);
    window.scroll(0,0);
    this.nombrePost = 0;
    document.getElementById("butonNewPost").style.display = "none";
  }


  loadMore(pEvent) {  
  
    setTimeout(() => {
      pEvent.target.complete();
      if(this.lstPost && this.lstPost.length > 19){      
      this.postService.getAllByFacilityUrl(this.nextUrlPost).then((data) => {
        var donne = data['hydra:member'] as Array<Post>;
        
        if(donne != null){
          this.lstPost = [...this.lstPost, ... data['hydra:member'] as Array<Post>];
        }
        if(data['hydra:view'] &&  data['hydra:view']['hydra:next']){
          this.nextUrlPost = data['hydra:view']['hydra:next'];
        }else{
          this.nextUrlPost = "";
        }
        if(data['hydra:view'] &&  data['hydra:view']['hydra:last']){
          this.lastUrlPost = data['hydra:view']['hydra:last'];
        }
       });
      }       
      
    }, 500);
  }


  
}
