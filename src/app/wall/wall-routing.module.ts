import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { NewPublicationComponent } from './new-publication/new-publication.component';
import { WallComponent } from './wall/wall.component';
import { GifComponent } from './new-publication/gif/gif.component';
import { PostComponent } from './post/post.component';

const routes: Routes = [
  { path: '', component: WallComponent },
  { path: 'publier', component: NewPublicationComponent },
  { path: 'gif', component: GifComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class WallRoutingModule { }
