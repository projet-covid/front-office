import { NgModule, Component } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../shared/shared.module';
import { Camera } from '@ionic-native/camera/ngx';
import { PhotoLibrary } from '@ionic-native/photo-library/ngx';

//ROUTER\\
import { WallRoutingModule } from './wall-routing.module';

//COMPONENTS\\
import { WallComponent } from './wall/wall.component';
import { NewPublicationComponent } from './new-publication/new-publication.component';
import { IonicModule } from '@ionic/angular';
import { from } from 'rxjs';
import { GifComponent } from './new-publication/gif/gif.component';
import { PostComponent } from './post/post.component';
import { MatDialogRef } from '@angular/material';


@NgModule({

  declarations: [
    WallComponent,
    NewPublicationComponent,
    PostComponent,
  ],
  imports: [
    CommonModule,
    IonicModule,
    WallRoutingModule,
    SharedModule,
  ],
  providers: [    
    Camera,
    PhotoLibrary,
  ]
})
export class WallModule { }
